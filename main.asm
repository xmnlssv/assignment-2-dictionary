%define READING_ERR_CODE 1
%define NO_MATCHES_ERR_CODE 2
%define SUCCESS_CODE 0
%define QWORD_BYTE_COUNT 8
%define BUFFER_SIZE 256
%define MAX_STRING_LENGTH 255

%include "words.inc"
%include "lib.inc"
%include "dict.inc"

section .rodata
reading_error_message: db "Failed while reading the word parameter.", 0
no_matches_error_message: db "No matches with passed string key in dictionary.", 0
        
section .bss
BUFFER: resb BUFFER_SIZE

section .text
global _start

_start:
        mov rdi, BUFFER
        mov rsi, BUFFER_SIZE

        call read_line
        test rax, rax
        jz .reading_error   

        push rdx
        mov rdi, rax
        mov rsi, first_word
        call find_word
        test rax, rax
        jz .no_matches_error   

        add rax, QWORD_BYTE_COUNT
        pop rdi
        add rdi, rax
        inc rdi
        call print_string
        jmp .success
        
.success:
        mov rdi, SUCCESS_CODE   
        jmp .exit

.reading_error:
        mov rdi, reading_error_message
        call print_error_string
        mov rdi,  READING_ERR_CODE
        jmp .exit

.no_matches_error:
        mov rdi, no_matches_error_message
        call print_error_string
        mov rdi, NO_MATCHES_ERR_CODE
.exit:
        add rsp, BUFFER_SIZE
        call exit

