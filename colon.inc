%define position 0

%macro colon 2
%ifid %2
        %2:
%else
%error "The label (second parameter) passed to the function is not correct"
%endif

%ifstr %1
        dq position
        db %1, 0
        %define position %2
%else
%error "The string (first parameter) passed to the function is not correct"
%endif
%endmacro

