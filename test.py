import subprocess
import sys

def checkInRealDict(keyStr):
    if len(keyStr) > 255:
        return (READING_ERROR_MESSAGE, 1)
    if realDict.get(keyStr):
        return (realDict.get(keyStr), 0)
    return (NO_MATCHES_ERROR_MESSAGE, 2)

def buildProject():
    subprocess.call(['make', 'clean'])
    subprocess.call(['make', 'program'])

def checkErrors(failed_test_dict):
    if(len(failed_test_dict) == 0):
        print("Success. Let's celebrate and...")
        print("Passed!")
    for i in failed_test_dict.keys():
        print("----------------------------")
        print(f"Failed in test '{str(i)}' with '{failed_test_dict[i][0]}' param.")
        print(f"program('{failed_test_dict[i][0]}') == '{failed_test_dict[i][1]}' (STDOUT)")
        print(f"program('{failed_test_dict[i][0]}') == '{failed_test_dict[i][2]}' (STDERR)")
        print(f"Expected - '{failed_test_dict[i][4]}' with return code '{failed_test_dict[i][3]}'")


inputs = ['first word', 'firstword', "word\n", "", "  ", "   ", "''", 'second word', 'third word', '  second word', 'third word ']
NO_MATCHES_ERROR_MESSAGE = 'No matches with passed string key in dictionary.'
READING_ERROR_MESSAGE = "Failed while reading the word parameter."

realDict = {
    'firstword': 'first word explanation',
    'second word': 'second word explanation',
    'third word': 'third word explanation'
}

input255 = ""
for i in range(254):
    input255 += "m"
inputs.append(input255)

input270 = ""
for i in range(270):
    input270 += "e"
inputs.append(input255)

failed_test_dict = dict()

buildProject()
for i in range(len(inputs)):
    popen = subprocess.Popen(["./program"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out_stream, err_stream = popen.communicate(input=inputs[i].encode())
    out_text = out_stream.decode().strip()
    err_text = err_stream.decode().strip()
    return_code = popen.returncode

    answ, answ_return_code = checkInRealDict(inputs[i])
    if not((return_code == 0 and out_text == answ and err_text == '') \
       or ((return_code == 1 or return_code == 2) and out_text == '' and err_text == answ)):
        print("F", end="")
        failed_test_dict[i + 1] = [inputs[i], out_text, err_text, return_code, answ]
    else:
        print(".", end="")
print()        
checkErrors(failed_test_dict)

