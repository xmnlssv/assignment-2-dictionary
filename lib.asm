%define WRITE_SYSCODE 1
%define STDOUT 1       
%define EXIT_SYSCODE 60        
%define STDERR 2

section .data

newline_char: db 10

section .text

global exit
global string_length
global print_string
global print_error_string
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word 
global read_line
global parse_uint
global parse_int
global string_copy

exit: 
    mov rax, 60         
    xor rdi, rdi         
    syscall
    ret      

string_length:
    xor rax, rax
    mov  rcx, rdi
.counter:
    cmp  byte [rcx], 0
    je   .end
    inc  rcx
    jmp  .counter
.end:
    sub  rcx, rdi
    mov  rax, rcx
    ret

print_string:
        mov rsi, STDOUT

print_string_with_cust_descr:
        push rsi
        push rdi
        call string_length
        pop rsi
        pop rdi
        mov rdx, rax
        mov rax, WRITE_SYSCODE 
        syscall
        ret

print_error_string:
        mov rsi, STDERR
        jmp print_string_with_cust_descr

print_char:
    xor rax, rax
   push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rax, 1
    mov rdi, 1
    syscall
    pop rdi
    ret

print_newline:
    xor rax, rax
    push 0xA ; пробел
    mov rdx, 1
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    syscall
    ret

print_int:
    test rdi, rdi
	jns print_uint
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi

print_uint:
	mov rax, rdi
	push rbx
	mov rbx, 10
	xor rcx, rcx
.push_loop:
	xor rdx, rdx
	div rbx
	add rdx, '0'
	push rdx
	inc rcx
	test rax, rax
	jnz .push_loop
.print_loop:
	pop rdi
	push rcx
	call print_char
	pop rcx
	dec rcx
	test rcx, rcx
	jnz .print_loop
	pop rbx
	ret    

string_equals:
    xor rax, rax
    xor rdx, rdx
.loop:
    mov ch, [rdi + rdx]
    cmp ch, byte[rsi + rdx]
    jne .end
    test ch, ch
    je .equals
    inc rdx
    jmp .loop
.equals:
    mov rax, 1
.end:    
    ret

string_equalss:
        xor rcx, rcx
.loop_on_string:
        mov r9b, [rdi + rcx]
        mov r10b, [rsi + rcx]    
        cmp r9b, r10b
        jnz .return_zero
        test r9b, r9b
        jz .return_one
        inc rcx
        jmp .loop_on_string
.return_one:
        mov rax, 1
        jmp .return_result
.return_zero:
        xor rax, rax
.return_result:
        ret 

read_char:
    sub rsp, 8   
    mov rsi, rsp       
    xor rdi, rdi 
    mov rdx, 1        
    mov rax, 0        
    syscall           
    cmp rax, 1
    jne end_read
    movzx rax, byte [rsp]
    jmp finish
end_read:
    xor rax, rax    
finish:
    add rsp, 8 
    ret

read_word:
	push r12
	push r13
	push r14
	push rsi
	push rdi
.space_loop:
	call read_char
	cmp al, `\t`
	je .space_loop
	cmp al, `\n`
	je .space_loop
	cmp al, ` `
	je .space_loop

	pop r12
	pop r13
	xor r14, r14
.word_loop:
	cmp al, `\t`
	je .term
	cmp al, `\n`
	je .term
	cmp al, ` `
	je .term
	test rax, rax
	jz .term
	cmp r14, r13
	jg .err
	mov [r12 + r14], al
	inc r14
	call read_char
	jmp .word_loop
.err:
	xor rax, rax
	xor rdx, rdx
	jmp .origin
.term:
	mov byte[r12 + r14], 0
	mov rax, r12
	mov rdx, r14
.origin:
	pop r14
	pop r13
	pop r12
	ret

read_line:
        push rbx
        xor rax, rax
        xor rdx, rdx
        xor rbx, rbx
        push rdx
        push rsi
        push rdi
.readloop:
        call read_char
        test rax, rax          
        jz .return_success
        jl .return_fail
        cmp rax, `\n`
        jz .skip_char        
.check_length:
        cmp rbx, [rsp + 8]
        jge .return_fail        
.store_char:
        pop rdi
        mov byte [rdi + rbx], al
        push rdi
        inc rbx
        jmp .readloop
.skip_char:
        test rbx, rbx
        jnz .return_success
        jmp .readloop
.return_success:
        pop rdi
        mov byte [rdi + rbx], 0
        mov rax, rdi
        mov rdx, rbx
        jmp .popping_n_return
.return_fail:
        pop rdi
        mov rdx, rbx
        xor rax, rax
.popping_n_return:
        pop rsi
        pop rsi
.return:
        pop rbx
        ret

parse_uint:
        xor rax, rax
        xor rcx, rcx
        xor r8, r8
        mov r9, 10
.loop:
        mov r8b, byte [rdi + rcx]
        cmp r8b, '0'
        jb .return
        cmp r8b, '9'
        ja .return
        sub r8b, '0'
        mul r9
        add rax, r8
        inc rcx
        jmp .loop
.return:
        mov rdx, rcx
        ret	
        
parse_int:
        xor rax, rax
        xor rdx, rdx
        cmp byte [rdi], '-'
        jz .read_negative_num
        cmp byte [rdi], '+'
        jz .read_positive_num
        call parse_uint
        jmp .return_result
.read_positive_num:
        inc rdi
        call parse_uint
        test rdx, rdx
        jz .return_result
        inc rdx
        jmp .return_result
.read_negative_num:
        inc rdi
        call parse_uint
        test rdx, rdx
        jz .return_result
        inc rdx
        neg rax
.return_result:
        ret 

        xor rax, rax
.loop:
        cmp rax, rdx
        jz .return_zero
        mov r9b, [rdi + rax]
        mov [rsi + rax], r9b
        inc rax
        test r9b, r9b
        jz .return
        jmp .loop
.return_zero:
        xor rax, rax
.return:
        ret
