%include "lib.inc"

section .text
global find_word

; searchs for a string among dictionary keys
; if any, returns pointer to the value of pointer to start of match, else 0
; params: 1st - search key str; 2nd - pointer to start of dictionary 
find_word:
        test rdi, rdi           ; if null string was passed
        jz .ret_no_matches

.loop:
        test rsi, rsi
        jz .ret_no_matches

        push rdi 
        push rsi
        lea rsi, [rsi + 8]      ; to reach the str value
        call string_equals
        pop rsi
        pop rdi

        test rax, rax           ; if didn't match back to .loop 
        jz .next_elem

        mov rax, rsi        ; if found search key str value go to .ret
        jmp .ret

.next_elem:
        mov rsi, [rsi]
        jmp .loop

.ret_no_matches:
        xor rax, rax
.ret:
        ret

